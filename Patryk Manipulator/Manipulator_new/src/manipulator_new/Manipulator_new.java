/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manipulator_new;

import com.sun.j3d.utils.applet.MainFrame;
import com.sun.j3d.utils.behaviors.mouse.MouseRotate;
import com.sun.j3d.utils.behaviors.mouse.MouseTranslate;
import com.sun.j3d.utils.behaviors.mouse.MouseWheelZoom;
import com.sun.j3d.utils.behaviors.vp.OrbitBehavior;
import com.sun.j3d.utils.geometry.Box;
import com.sun.j3d.utils.geometry.Cone;
import com.sun.j3d.utils.geometry.Cylinder;
import com.sun.j3d.utils.geometry.Sphere;
import com.sun.j3d.utils.image.TextureLoader;
import com.sun.j3d.utils.universe.SimpleUniverse;
import com.sun.xml.internal.bind.v2.schemagen.xmlschema.Appinfo;
import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.GraphicsConfiguration;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.media.j3d.Appearance;
import javax.media.j3d.Background;
import javax.media.j3d.BoundingSphere;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.Canvas3D;
import javax.media.j3d.ColoringAttributes;
import javax.media.j3d.ImageComponent;
import javax.media.j3d.ImageComponent2D;
import javax.media.j3d.Locale;
import javax.media.j3d.Material;
import javax.media.j3d.PointLight;
import javax.media.j3d.Texture;
import javax.media.j3d.Texture2D;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;
import javax.media.j3d.WakeupOnCollisionEntry;
import javax.media.j3d.WakeupOnCollisionExit;
import javax.swing.Timer;
import javax.vecmath.Matrix3d;
import javax.vecmath.Matrix4f;
import javax.vecmath.Vector3d;


/**
 *
 * @author Patryk
 */
class Manipulator_new extends Applet implements ActionListener, KeyListener
{
    private int u=100;
    private Matrix3d macioryRecords[] = new Matrix3d[u];
    private int licznik=0;
    private int tempInt=0;
    private int tempInt2=0;
    private boolean nagrywanieIsOn = true;
    private boolean odtwarzanieTrwa = false;
    Transform3D przesuniecie_obserwatora1 = new Transform3D();
    Transform3D obrot_obserwatora = new Transform3D();
    Transform3D obrot_robota = new Transform3D();               // obrot w talii
    Transform3D obrot_robota2 = new Transform3D();              //obrot w barku
    Transform3D obrot_robota3 = new Transform3D();              //obrot w łokciu
    Transform3D przes2 = new Transform3D();                     //przesuniecie trans2 (od obrotu w barku)
     Transform3D przesuniecie_belki2;
     Transform3D przesuniecie_pilki;
                   //przesuniecie trans2 (od obrotu w barku)
    Transform3D przes3 = new Transform3D(); 
    BranchGroup wezel_scena;
    BranchGroup proba;
    public int ktoryOstatni=10;
    CollisionDetector wykrywacz;
    boolean zlapany = false;
    boolean logicznaWartosc = false;
    Cylinder magnes;
    TransformGroup przes_m;
 
    int a = 0;
Sphere pilka;
    
//przesuniecie trans3 (od obrotu w lokciu)
                //przesuniecie trans3 (od obrotu w lokciu)
    //Transform3D obrot_robota2 = new Transform3D();              //obrot w barku
    //Transform3D obrot_robota3 = new Transform3D();              //obrot w łokciu
    
    TransformGroup trans1 , trans2, trans3, trans_przegub1, trans_przegub2,przes_k;
    TransformGroup scena_calosc;
    SimpleUniverse wszechswiat;
    //??????
    int kierunek_w_talii, kierunek_w_barku, kierunek_w_lokciu;
    double kat_t=2.0f,kat_b,kat_l;                                   //kat obrotu w poszczegolnych stawach
    
     public void obrotTaliaLewo(float krok)
    {
        if (!(ktoryOstatni==1 && wykrywacz.inCollision==true))
            kat_t -=krok; 
            obrot_robota.rotY(kat_t);
            trans1.setTransform(obrot_robota);
            ktoryOstatni=1;
            
         
    }
     public void przyczep_pilke()
    {
           
           logicznaWartosc=!logicznaWartosc;
           if(logicznaWartosc)
           {
           System.out.println("alt");
           wezel_scena.removeChild(proba);
           przesuniecie_pilki.set(new Vector3f(0.0f, 0.157f, 0.0f));
           przes_k.setTransform(przesuniecie_pilki);
           przes_m.addChild(proba);
           
           }
           else
           {
           System.out.println("al22222t");
           przes_m.removeChild(proba);
           przesuniecie_pilki.set(new Vector3f(0.0f,-0.65f,0.6f));
           przes_k.setTransform(przesuniecie_pilki);
            
           
           przes_k.setTransform(przesuniecie_pilki);
           wezel_scena.addChild(proba);
           
           }
           
           
           
           
        
       
    }
    public void obrotTaliaPrawo(float krok)
    {
         if (!(ktoryOstatni==2 && wykrywacz.inCollision==true))
            kat_t +=krok; 
            obrot_robota.rotY(kat_t);
            trans1.setTransform(obrot_robota);
            ktoryOstatni=2;
    }
    public void obrotBarkLewo(float krok)
    {
         if (!(ktoryOstatni==3 && wykrywacz.inCollision==true))
            if(kat_b>-2.0)
            {
                kat_b -=krok; 
                obrot_robota2.rotY(kat_b);  
                przes2.setTranslation(new Vector3f(0.0f,-0.05f,-0.2f)); 
                obrot_robota2.mul(przes2); 
                trans2.setTransform(obrot_robota2);
                //nagrywanie
                if(nagrywanieIsOn==true){
                    obrot_robota2.get(macioryRecords[licznik]);
                    licznik++;
                }
                
            }
         ktoryOstatni=3;
    }
    public void obrotBarkPrawo(float krok)
    {
         if (!(ktoryOstatni==4 && wykrywacz.inCollision==true))
            if(kat_b<2.0 && kat_l+kat_b<2.3f)                //żeby ramię się nie zderzało z resztą robota
            {
                kat_b +=krok; 
                obrot_robota2.rotY(kat_b);  
                przes2.setTranslation(new Vector3f(0.0f,-0.05f,-0.2f)); 
                obrot_robota2.mul(przes2);  
                trans2.setTransform(obrot_robota2);
            }
         ktoryOstatni=4;
    }
    public void obrotLokiecLewo(float krok)
    {
         if (!(ktoryOstatni==5 && wykrywacz.inCollision==true))
        if(kat_l>-3.5f) 
        {
            kat_l -= krok;
            obrot_robota3.rotY(kat_l);
            przes3.setTranslation(new Vector3f(0.0f,0.08f,0.00f));
            obrot_robota3.mul(przes3);
            trans3.setTransform(obrot_robota3);
        }
         ktoryOstatni=5;
    }
    
    public void obrotLokiecPrawo(float krok)
    {
         if (!(ktoryOstatni==6 && wykrywacz.inCollision==true))
        if(kat_l<1 && kat_l+kat_b<2.3) 
        {
            kat_l += krok;
            obrot_robota3.rotY(kat_l);
            przes3.setTranslation(new Vector3f(0.0f,0.08f,0.00f));
            obrot_robota3.mul(przes3);
            trans3.setTransform(obrot_robota3);
        }
         ktoryOstatni=6;
    }
   
    Manipulator_new()
    {
        setLayout(new BorderLayout()); // ustawienie layoutu
        GraphicsConfiguration konfiguracja = SimpleUniverse.getPreferredConfiguration();
        
        Canvas3D canvas = new Canvas3D(konfiguracja);
        add(BorderLayout.CENTER, canvas);
        canvas.addKeyListener(this);
       
        Panel p = new Panel();
        
        wszechswiat = new SimpleUniverse(canvas);
             
             OrbitBehavior orbit = new OrbitBehavior(canvas, OrbitBehavior.REVERSE_ROTATE);
            orbit.setSchedulingBounds(new BoundingSphere());
         wszechswiat.getViewingPlatform().setViewPlatformBehavior(orbit);
        przesuniecie_obserwatora1.set(new Vector3f(0.0f,-0.2f,4.0f));
        
        //wszechswiat.getViewingPlatform().getViewPlatformTransform().setTransform(przesuniecie_obserwatora1);
        BranchGroup scene = UtworzScene(wszechswiat);
        wszechswiat.addBranchGraph(scene);
    }
    //-----------------------------------------TWORZENIE SCENY--------------------------------------------------------------
    public BranchGroup UtworzScene(SimpleUniverse su)
    {
         
        wezel_scena = new BranchGroup();
        TransformGroup widok_kamery = null;
        widok_kamery = su.getViewingPlatform().getViewPlatformTransform();
        
        //--------------------------------------tworzenie robota------------------------------------------
        Material mat = new Material
        (new Color3f(1.0f, 0.0f, 0.0f), new Color3f(0.0f, 0.0f, 0.0f), 
                new Color3f(1.0f, 0.0f, 0.0f), new Color3f(1.0f, 1.0f, 1.0f), 40f);
        Appearance robot = new Appearance();
        
        
        TextureLoader blacha = new TextureLoader("blacha.jpg", null);
        ImageComponent obrazek =  blacha.getImage();
        
        Texture2D  blaszka= new Texture2D(Texture.BASE_LEVEL,Texture.RGBA, obrazek.getWidth(), obrazek.getHeight());
        
        blaszka.setImage(0, obrazek);
        blaszka.setBoundaryModeS(Texture.WRAP);
        blaszka.setBoundaryModeT(Texture.WRAP);
        
        robot.setTexture(blaszka);
        //--------------------------------------wyglad kuli -----------------------------------------
        Material mat_kuli= new Material(new Color3f(1.0f, 0.1f, 0.0f), new Color3f(0.3f, 0.0f, 0.0f), 
                new Color3f(1.0f, 0.7f, 0.0f), new Color3f(1.0f, 1.0f, 1.0f), 40f);
        Appearance kula = new Appearance();
        kula.setMaterial(mat_kuli);
        
        
        
        
        
        
        
        BoundingSphere bounds = new BoundingSphere();
        
        scena_calosc = new TransformGroup();
        scena_calosc.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        trans1 = new TransformGroup();
        trans1.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        
        //--------------------nagrywanko---------------
        for(int i =0; i<100; i++)
            macioryRecords[i] = new Matrix3d();
        
        
        
        //---------------------------------TŁO------------------------------------------------------
        Background background = new Background(new Color3f(1f,1,1));
        BoundingSphere sphere = new BoundingSphere(new Point3d(0.0f,0.0f,0.0f), 100000);
        background.setApplicationBounds(sphere);
        trans1.addChild(background);
        //----------------------------cylinder - noga--------------------------------------------------
        
        Cylinder noga = new Cylinder(0.1f,0.3f,Cylinder.GENERATE_NORMALS|Cylinder.GENERATE_TEXTURE_COORDS, robot);
        noga.setAppearance(robot);
        Transform3D przesuniecie_nogi = new Transform3D();
        przesuniecie_nogi.set(new Vector3f(0.0f,-0.65f,0.0f));
        TransformGroup przes_n = new TransformGroup(przesuniecie_nogi);
        scena_calosc.addChild(przes_n);
        przes_n.addChild(noga);
        Cylinder talia = new Cylinder(0.15f,0.1f,Cylinder.GENERATE_NORMALS|Cylinder.GENERATE_TEXTURE_COORDS, robot);
        //-------------------------------cylinder - talia--------------------------------------------
        talia.setAppearance(robot);
        Transform3D przesuniecie_talii = new Transform3D();
        przesuniecie_talii.set(new Vector3f(0.0f,-0.45f,0.0f));
        TransformGroup przes_t = new TransformGroup(przesuniecie_talii);
        trans1.addChild(przes_t);
        przes_t.addChild(talia);
        //----------------------------cylinder - brzuch--------------------------------------------------
        Cylinder brzuch = new Cylinder(0.15f,0.2f,Cylinder.GENERATE_NORMALS|Cylinder.GENERATE_TEXTURE_COORDS, robot);
        brzuch.setAppearance(robot);
        Transform3D przesuniecie_brzucha = new Transform3D();
        przesuniecie_brzucha.set(new Vector3f(0.0f,-0.25f,0.0f));
        Transform3D obrot_staly_brzucha = new Transform3D();
        obrot_staly_brzucha.rotX(Math.PI/2);
        przesuniecie_brzucha.mul(obrot_staly_brzucha);
        TransformGroup przes_b = new TransformGroup(przesuniecie_brzucha);
        trans1.addChild(przes_b);
        przes_b.addChild(brzuch);
        //----------------------- Cylinder-przegub1 - OBRÓT W BARKU WZGLEDEM TEGO!!!-------------------------------
        Cylinder przegub1 = new Cylinder(0.05f, 0.01f,Cylinder.GENERATE_NORMALS|Cylinder.GENERATE_TEXTURE_COORDS, robot);
        Transform3D  przes_przegub1   = new Transform3D();   
        przes_przegub1.set(new Vector3f(0.0f,-0.25f,-0.1f));
        Transform3D obrot_staly_przegubu = new Transform3D();
        obrot_staly_przegubu.rotX(Math.PI/2);
        przes_przegub1.mul(obrot_staly_przegubu);
        trans_przegub1 = new TransformGroup(przes_przegub1);
        trans_przegub1.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        trans_przegub1.addChild(przegub1);
        //------------------------Cylinder - belka - ramie -------------------------------------------------------
        Box  belka = new Box(0.08f,0.05f,0.2f,Box.GENERATE_NORMALS|Box.GENERATE_TEXTURE_COORDS,  robot);
        Transform3D przesuniecie_belki = new Transform3D();
        przesuniecie_belki.set(new Vector3f(0.0f,-0.05f,-0.2f));
        trans2 = new TransformGroup(przesuniecie_belki);
        trans2.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        trans2.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
        trans2.addChild(belka);   
        //------------------------Cylinder - bark1 i łokieć--------------------------------------------------------
        Cylinder bark_c1 = new Cylinder(0.08f,0.1f,Cylinder.GENERATE_NORMALS|Cylinder.GENERATE_TEXTURE_COORDS, robot);
        Cylinder bark_c2 = new Cylinder(0.08f,0.1f,Cylinder.GENERATE_NORMALS|Cylinder.GENERATE_TEXTURE_COORDS, robot);
        bark_c1.setAppearance(robot);
        bark_c2.setAppearance(robot);
        Transform3D przesuniecie_barku_c1 = new Transform3D();
        przesuniecie_barku_c1.set(new Vector3f(0.0f,0.0f,0.2f));
        Transform3D przesuniecie_barku_c2 = new Transform3D();
        przesuniecie_barku_c2.set(new Vector3f(0.0f,0.0f,-0.2f));
        TransformGroup przes_bc1 = new TransformGroup(przesuniecie_barku_c1);
        TransformGroup przes_bc2 = new TransformGroup(przesuniecie_barku_c2);
        trans2.addChild(przes_bc1);
        trans2.addChild(przes_bc2);
        przes_bc1.addChild(bark_c1);
        przes_bc2.addChild(bark_c2);
         //----------------------- Cylinder-przegub2 - OBRÓT W ŁOKCIU!!!-------------------------------
        Cylinder przegub2 = new Cylinder(0.05f, 0.01f, robot);
        Transform3D  przes_przegub2   = new Transform3D();   
        przes_przegub2.set(new Vector3f(0.0f,0.05f,-0.2f));
        trans_przegub2 = new TransformGroup(przes_przegub2);
        trans_przegub2.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        trans_przegub2.addChild(przegub2);
                     
        //------------------------Cylinder - belka - przedramie --------------------------------------------------------
        Box  belka2 = new Box(0.2f,0.08f,0.08f,Box.GENERATE_NORMALS|Box.GENERATE_TEXTURE_COORDS, robot);        
        przesuniecie_belki2 = new Transform3D();
        przesuniecie_belki2.set(new Vector3f(0.0f,0.08f,0.00f));        
        trans3=new TransformGroup(przesuniecie_belki2);
        trans3.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        trans3.setCapability(TransformGroup.ALLOW_CHILDREN_EXTEND);
        trans3.addChild(belka2);
        
        //------------------------Stożek  -  --------------------------------------------------------
        Cone stozek = new Cone(0.08f,0.1f,Cone.GENERATE_NORMALS|Cone.GENERATE_TEXTURE_COORDS, robot);
        stozek.setAppearance(robot);
        Transform3D przesuniecie_stozka = new Transform3D();
        przesuniecie_stozka.set(new Vector3f(-0.25f,0.0f,0.0f));
        Transform3D obrot_stozka = new Transform3D();
        obrot_stozka.rotZ(Math.PI/2);
        przesuniecie_stozka.mul(obrot_stozka);
        TransformGroup przes_s = new TransformGroup(przesuniecie_stozka);
        trans3.addChild(przes_s);
        przes_s.addChild(stozek);
        
        
        Timer odtwarzanie = new Timer(20, this);
        
        
        
        
        
        //------------------------pilka  -  --------------------------------------------------------
    
            pilka = new Sphere(0.15f);
        pilka.setAppearance(kula);
         przesuniecie_pilki = new Transform3D();
        przesuniecie_pilki.set(new Vector3f(0.0f,-0.65f,0.6f));
        przes_k = new TransformGroup(przesuniecie_pilki);
        przes_k.addChild(pilka);
         przes_k.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
        przes_k.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
       // wezel_scena.addChild(przes_k);
         proba = new BranchGroup();
         
         proba.setCapability(BranchGroup.ALLOW_CHILDREN_WRITE);
         proba.setCapability(BranchGroup.ALLOW_CHILDREN_READ);
         proba.setCapability(BranchGroup.ALLOW_DETACH);
         proba.setCapability(BranchGroup.ALLOW_CHILDREN_EXTEND);
        proba.addChild(przes_k);
        wezel_scena.setCapability(BranchGroup.ALLOW_CHILDREN_WRITE);
        wezel_scena.setCapability(BranchGroup.ALLOW_CHILDREN_READ);
        wezel_scena.setCapability(BranchGroup.ALLOW_DETACH);
        wezel_scena.setCapability(BranchGroup.ALLOW_CHILDREN_EXTEND);
        
        wezel_scena.addChild(proba);
        
        
        
        
        //---------
        Matrix3d tempMat = new Matrix3d();
        przesuniecie_pilki.get(tempMat);
        
        
        //-----------
        
        
        
        wykrywacz = new CollisionDetector(pilka,new BoundingSphere(new Point3d(0, 0, 0), 0.03d));
        wykrywacz.setSchedulingBounds(bounds);
        scena_calosc.addChild(wykrywacz);
        
        
        
        
        
        
        
        //------------------------cylinder - magnes?-----------------------------------------
         magnes = new Cylinder(0.05f,0.02f);
        Appearance wyglad_magnesu = new Appearance();
        Material mat2 = new Material(new Color3f(0.1f, 0.1f, 0.1f), new Color3f(0.0f, 0.0f, 0.0f), new Color3f(0.1f, 0.1f, 0.1f), new Color3f(1.0f, 1.0f, 1.0f), 70f);
        wyglad_magnesu.setMaterial(mat2);
        magnes.setAppearance(wyglad_magnesu);
        Transform3D przesuniecie_magnesu = new Transform3D();
        przesuniecie_magnesu.set(new Vector3f(-0.3f,0.00f,0.0f));
        Transform3D obrot_magnesu = new Transform3D();
        obrot_magnesu.rotZ(Math.PI/2);
        przesuniecie_magnesu.mul(obrot_magnesu);
         przes_m = new TransformGroup(przesuniecie_magnesu);
         przes_m.setCapability(TransformGroup.ALLOW_CHILDREN_EXTEND);
         przes_m.setCapability(TransformGroup.ALLOW_CHILDREN_WRITE);
         przes_m.setCapability(TransformGroup.ALLOW_CHILDREN_READ);
        trans3.addChild(przes_m);
        przes_m.addChild(magnes);
        //------------------------Podloga --------------------------------------------------------        
        
        
        Appearance wyglad_podlogi = new Appearance();
        
        TextureLoader tekstura = new TextureLoader("trawa.jpg",null);
        ImageComponent2D obraz = tekstura.getImage();
        
        Texture2D trawunia = new Texture2D(Texture.BASE_LEVEL, Texture.RGBA, obraz.getWidth(), obraz.getHeight());
        trawunia.setImage(0, obraz);
        trawunia.setBoundaryModeS(Texture.WRAP);
        trawunia.setBoundaryModeT(Texture.WRAP);
        
        
        
        //Material matp = new Material(new Color3f(0.0f, 1.0f, 0.0f), new Color3f(0.0f, 0.0f, 0.0f), new Color3f(0.0f, 1.0f, 0.0f), new Color3f(1.0f, 1.0f, 1.0f), 70f);
        wyglad_podlogi.setTexture(trawunia);
        Box  podloga = new Box(4.0f,4.0f,0.1f,Box.GENERATE_NORMALS|Box.GENERATE_TEXTURE_COORDS, wyglad_podlogi);
        Transform3D przesuniecie_podlogi = new Transform3D();
        przesuniecie_podlogi.set(new Vector3f(0.0f,-0.9f,0.0f));
        Transform3D obrot_podlogi = new Transform3D();
        obrot_podlogi.rotX(Math.PI/2);
        przesuniecie_podlogi.mul(obrot_podlogi);
        TransformGroup przes_podlogi = new TransformGroup(przesuniecie_podlogi);
        scena_calosc.addChild(przes_podlogi);
        przes_podlogi.addChild(podloga);
        //-----------------------------------Światło------------------------------------
        
        PointLight swiatlozprzodu = new PointLight();
        swiatlozprzodu.setInfluencingBounds(new BoundingSphere(new Point3d(0.0, 0.0, 0.0), 100.0f));
        swiatlozprzodu.setPosition(new Point3f((float) -3.0f, 3.0f, 5.0f));
        swiatlozprzodu.setColor(new Color3f(1.0f, 1.0f, 1.0f));
        
        wezel_scena.addChild(swiatlozprzodu);
        
        //-----------------------kto czyim dzieckiem-----------------------------------------
        
        wezel_scena.addChild(scena_calosc);
        scena_calosc.addChild(trans1);
        trans1.addChild(trans_przegub1);
        trans_przegub1.addChild(trans2);
        trans2.addChild(trans_przegub2);
        trans_przegub2.addChild(trans3);
        
        
        //----------------------------------MYSZKA------------------------------------------
       /* MouseRotate wholeSceneMouseRotator = new MouseRotate();
        wholeSceneMouseRotator.setTransformGroup(scena_calosc);
        wholeSceneMouseRotator.setSchedulingBounds(new BoundingSphere());
        wezel_scena.addChild(wholeSceneMouseRotator);*/
    
        /*MouseWheelZoom wholeSceneMouseZoom = new MouseWheelZoom();
        wholeSceneMouseZoom.setTransformGroup(widok_kamery);
        wholeSceneMouseZoom.setSchedulingBounds(new BoundingSphere());
        wezel_scena.addChild(wholeSceneMouseZoom);
       */
            
    
       /* MouseTranslate wholeSceneMouseTranslate = new MouseTranslate();
        wholeSceneMouseTranslate.setTransformGroup(scena_calosc);
        wholeSceneMouseTranslate.setSchedulingBounds(new BoundingSphere());
        wezel_scena.addChild(wholeSceneMouseTranslate);*/
        
        obrot_robota.rotY(kat_t);
        trans1.setTransform(obrot_robota);
        
        wezel_scena.compile();
        return wezel_scena;
    }
    //---------------------------KEY LISTENERY-------------------------------------------------------------------------
    @Override
    public void actionPerformed(ActionEvent e) {
        if(odtwarzanieTrwa==true && nagrywanieIsOn==false){
                    
                    if(licznik!=0){
                        tempInt = licznik;
                    }    
                    obrot_robota2.set(macioryRecords[tempInt2]);
                    trans2.setTransform(obrot_robota2);
                    tempInt2++;
                    if(tempInt2 == tempInt){
                        odtwarzanieTrwa=false;
                        tempInt2=0;
                    }
                }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) 
    {
        switch(e.getKeyCode())
        {
            case KeyEvent.VK_A:
                obrotTaliaLewo(0.1f);break;
            case KeyEvent.VK_D:
                obrotTaliaPrawo(0.1f);break;
            case KeyEvent.VK_LEFT:
                obrotBarkLewo(0.1f);break;
            case KeyEvent.VK_RIGHT:
                obrotBarkPrawo(0.1f);break;
            case KeyEvent.VK_UP:
                obrotLokiecLewo(0.1f);break;
            case KeyEvent.VK_DOWN:
                obrotLokiecPrawo(0.1f);break;
            case KeyEvent.VK_SPACE:
            {
                if (CollisionDetector.inCollision==true || proba.getParent()==przes_m)
                przyczep_pilke();
                break;
            }
            case KeyEvent.VK_8:
                odtwarzanieTrwa=true;break;
            case KeyEvent.VK_7:
                nagrywanieIsOn=false;break;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }
    

 
    public static void main(String[] args) 
    {
        // TODO code application logic here
        Manipulator_new robot = new Manipulator_new();
        robot.addKeyListener(robot);
        MainFrame mf = new MainFrame(robot, 800, 600); 
    }

}